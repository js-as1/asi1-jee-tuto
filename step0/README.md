# Création d'un projet JEE

## 1. Contexte

Une application Web Java va donner lieu à la création d'une archive ```myProject.war``` qui va contenir l'ensemble des fichiers java pré-compilés (```*.class```) mais aussi l'ensemble des fichiers statiques de l'application web (```*.css,*.js,*.html,*.jsp,...```). Cette archive sera ensuite déposée sur une serveur Web (```e.g Apache Tomcat```) qui permettra de démarrer l'application.

---
## 2. Création sous Eclipse
### 2.1 Télécharger Eclipse 
l'IDE eclipse est disponible au lien ci-dessous:

```
https://www.eclipse.org/downloads/packages/
```

### 2.2 Démarrer Eclipse

### 2.3 Sélectionner un workspace
 Le workspace représente l'endroit où l'ensemble des projets java que vous allez créez/manipuler seront référencés. Un dossier ```.metadata``` sera alors créé. Il contiendra l'ensemble des outils et des références aux projets que vous allez utiliser.

<img alt="img sélection d'un workspac" src="images/ws-select.jpg" width="400">

### 2.4 Création d'un projet Web Dynamic

- Ouvrir le menu et suivre le déroulé ci-après: ```File > New > Dynamic Web Project```


<img alt="img Web Dynamic Project Creation" src="images/dynamicWebProjectCreation.jpg" width="400">


- **Project name:** nom du projet.
- **Project location:** endroit ou sera stocké les fichiers du projet. Par défault dans le répertoire du workspace.
- **Target runtime:** server Web sur lequel l'application sera déployée (e.g Apache Tomcat). 
- **Dynamic web module version:** version de projet Web dynamique (les spécifications et outils changent d'une version à une autre, ici nous allons choisir la version 3.0).
- **Configuration:** configurations additionnelles possibles lors d'usage d'outils (e.g jsf)
- **EAR membership:** lors de la création de gros projets JEE plusieurs projets peuvent être regroupés entre eux (Application Web, Projet Machine to Machine M2M, etc...) si votre projet est autonome ne pas sélectionner cette rubrique.
- **Working sets:** dans eclipse les projets peuvent être organisés en working sets (simple groupe de projets), pas d'impact sur le projet seulement sur la visualisation des projets dans eclipse.

Votre projet doit alors se présenter sous la forme suivante:

<img alt="img Web Dynamic Project Creation" src="images/webDynamicProjectTree.jpg" width="200">

Sur le schema ci-dessus des packages et fichiers ont été ajoutés pour illustrer les différents dossiers. 
Explications:
- ``` Java Resources/src ``` : ensemble des packages et fichier java qui vont être pré-compilé, Objets java purs:
  - Servlet (Contrôleur de l’application)
  - Java Bean (gestion du Modèle)
  - Objets java
-  ``` WebContent ```: Conteneur des informations Web complémentaires
   - Informations Web classiques statiques
		- *.css
		- *.js
		- *.html, *.xhtml
   - Information Web Compilée
     - *.jsp ( ou d'autres e.g *.jsf)
   - Métadonnées et données complémentaires
		- WEB INF/lib : jar complémentaires
		- WEB INF/web.xml: fichiers de configuration de notre application WEB.
---

## 3 Création sous Eclipse avec Maven

### 3.1 Création d'un projet Maven

- Ouvrir le menu et suivre le déroulé ci-après: ```File > New > Maven Project```
- Sélectionner la checkbox ```Create a simple project (skip archetype selection) ```

- Compléter les informations en spécifiant le mode de packaging ``` war ```
<img alt="img Web Dynamic Project Creation" src="images/MavenProjectCreation.jpg" width="300">

- Cliquer sur finish
- Ouvrir le fichier ```pom.xml``` dans l'arborescence générée
  - remplacer ```<groupId>``` par votre groupeId (e.g com.cpe.tuto.jee)
  - remplacer ```<artefactId>``` par votre groupeId (e.g MavenWebDynamicProject)

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.cpe.tuto.javaee</groupId>
	<artifactId>WebDynamicMavenProjectTuto</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>war</packaging>
	<dependencies>
		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-api</artifactId>
			<version>7.0</version>
			<scope>provided</scope>
		</dependency>
	</dependencies>
	<build>
		<defaultGoal>clean install</defaultGoal>
		<plugins>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
					<failOnMissingWebXml>false</failOnMissingWebXml>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<configuration>
					<failOnMissingWebXml>false</failOnMissingWebXml>
				</configuration>
			</plugin>
		</plugins>
	</build>
</project>

```

### 3.2 Configuration du projet Maven
- Afin que eclipse reconnaisse le répertoire qui contient un contenu Web  (*.html, *.js, *.css, *.jsp) il est nécessaire de faire une opération supplémentaire
- Clic droit sur le projet puis ```run -> Maven Clean``` (nettoyage des répos et téléchargement des dépendances)
- Clic droit sur le projet ``` Properties -> Java Build Path ```
  - Sélectionner l'onglet ```Source ```
  - Cliquer sur ```Add Folder```
  - Sélectionner le répertoire ```src/main/webapp```

<img alt="img Web Dynamic Project Creation" src="images/MavenProjectCreationAddWebContent.jpg" width="300">

- Le répertoire ```webapp``` est maintenant accessible depuis l'IDE Eclipse
- Dans le répertoire ```webapp``, ajouter un fichier ```index.html```

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <title>INFO</title>
    </head>
    <body>
        <h1>INFO!!</h1>
    </body>
</html>

```
- Démarrer votre projet web dynamique:
  - Clic droit sur le projet ```Run -> Run on Server```, choisir un server apache Tomcat disponible


<img alt="img Web Dynamic Project Creation" src="images/MavenProjectResult.jpg" width="300">

---
## 4 Association d'un Serveur Web extérieur
- Afin d'exécuter notre application Web nous avons besoin d'associer un serveur Web extérieur qui chargera notre application Web (*.war).
- Choisir le menu ```File -> New -> Other```
- Dans le menu de rechercher taper ```server```. 
- Cliquer sur ```Next >```.

<img alt="new server" src="images/newServer.jpg" width="300">

- Dans la nouvelle fenêtre choisir un serveur de Type ```Apache```, ```Tomcat 8.5```.

<img alt="img Server Apache 1" src="images/ServerApache1.jpg" width="300">

- Cliquer sur ```Configure runtime environments...```

<img alt="img Server Apache 2" src="images/ServerApache2.jpg" width="300">

- Dans la nouvelle fenêtre cliquer sur ```Add``` et choisir le type de server à ajouter ```Tomcat 8.5``` et choisir la racine du répertoire de votre Apache tomcat.

<img alt="img Server Apache 2" src="images/ServerApache3.jpg" width="300">

- Valider tout, un serveur Apache Tomcat 8.5 est maintenant associé à votre Eclipse.
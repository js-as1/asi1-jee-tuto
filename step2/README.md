# JSP et JavaBean

## 1. Contexte

Les Servlets permettent d'intercepter des requêtes HTTP et de déclencher du code métier Java en fonction de la requête émise. Même s'il est possible directement depuis une Servlet de renvoyer un flux HTML au Web Browser, manipuler des balises HTML au sein d'un programme Java n'est une chose aisée. Créer des pages HTML dynamiquement incorporant des variables modifiées par le code métier (Java) est possible par l'intermédiaire de ```Java Server Page (JSP)```.
Les ```JSP``` ont la même fonctionnalité de rendu que les fichiers .php, elles vont représenter des templates HTML dans lesquels du contenu et des variables vont être dynamiquement ajoutés.

<p align="center"> 
<img alt="Kitchen Vs JEE 1" src="./images/KitchenJEE1.jpg" width="300">
<img alt="Kitchen Vs JEE 2" src="./images/KitchenJEE2.jpg" width="317">
</p>

Une analogie peut être faite entre un restaurant et le fonctionnement du Web Container JEE. Dans un restaurant les ```clients``` ont à leur disposition un ```menu``` afin de choisir leur repas. Le ```serveur``` prend la commande des ```clients``` et envoie les ```commandes``` en cuisine. Le ```chef``` traite ces commandes à l'aide de ```recettes``` de cuisine, d'```ustensiles```, de matières premières (```nourriture```) et renvoie ```les plats cuisinés```.
Dans un serveur JEE le fonctionnement est similaire:
- Le ```web browser``` (```client```) choisit une ```URL``` (```menu```) à destination d'un ```serveur HTTP JEE``` (```serveur```).
- Le ```server HTTP``` intercèpte les requêtes et les transforme en ```HTTP Request``` (```commandes```) à destination des ```Servlets``` (```chef```).
- Le ```Servlet``` utilise des outils JEE à sa disposition (```ustensiles```), des fichiers JSP (```recettes```) et des ```données``` (```nourriture```) pour fabriquer une réponse.
- Cette HTTP Response (```plats cuisinés```) est ainsi transmise au ```Web Browser```.

---

## 2. Création d'un Dynamic Web Project
Utiliser la section [Step0](../step0/README.md) pour créer un projet Web Dynamic portant le nom suivant : ```JSP-Servlet-JavaBean-Sample```

---

## 3. Création d'un JSP Simple
- Dans le répertoire ```WebContent``` du projet Web Dynamic créer un fichier ```poc.jsp``` 

```html
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Poc JSP PAGE</title>
</head>
<body>
	<h1>SUPER HEROS JSP PAGE CREATOR</h1>
	<h3>this page is generated from the server using the EL expression
		this is ${54 < 21 || 10>1}</h3>
	<ul>
		<li> name parameter value: ${param.name}</li>
		<li> name parameter value: ${param.surname}</li>
	</ul>
</body>
</html>
```

- Explications
    ```html
    <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>
    ```
  -  ```<% ...%>``` : Balise d'échappement permettant l'exécution de code java
  -  ```contentType="text/html"``` permet de spécifier le type MIME du document retourné au client
    ```html
    ${54 < 21 || 10>1}
    ```
  - ``` ${...} ``` contient des expressions qui sont évaluées et remplacées par le résultat de leur évaluation.
    ```html
    <ul>
        <li> name parameter value: ${param.name}</li>
        <li> name parameter value: ${param.surname}</li>
    </ul>
    ```
  - ```${param. ...}``` : dans les JSPs plusieurs variables sont mises à disposition pour permettre une interaction entre la JSP et la requête HTTP, entre la JSP et la Servlet et entre JSPs. Ci-dessous un liste non exhaustive des variables mises à disposition:
    - ```param``` : régroupe l'ensemble des paramètres de la requête HTTP
    - ```request``` : la requête HttpServletRequest
    - ```response``` : la réponse HttpServletResponse
    - ```session``` : instance de HttpSession
    - ```out```: le PrintWriter utilisé pour écrire sur la sortie
    - ```application``` : Cette variable correspond à l'objet ServletContext obtenu grâce à getServletConfig getServletContext (). Permet aux JSPs d'enregistrer des données persistantes
    - D'autres éléments sont disponibles permettant d'accéder à des espaces de mémoires partagés entre les objets Jee
      -  ```requestScope``` : Espace d'échange d'objets
      -  ```sessionScope``` : Espace d'échange d'objets
      -  ```applicationScope``` : Espace d'échange d'objets

- Lancer votre server Web et ouvrir l'URL suivante depuis un navigateur Web
  
```
http://localhost:8080/JSP-Servlet-JavaBean-Sample/poc.jsp?name=Doe&surname=John
```

Vous devriez obtenir le résultat suivant:
```
SUPER HEROS JSP PAGE CREATOR

this page is generated from the server using the EL expressionthis is true
• name parameter value: Doe
• name parameter value: John
```
---

## 4. Echange d'information

### 4.1 Différents types d'échanges

Il existe plusieurs façon d'interagir entre les composants JEE de type servlet ou JSP. Des containers d'information sont mis à disposition et disposent chacun d'une durée de vie différentes:
- ```Param```: paramètres de la requête HTTP qui ne peuvent être qu'au format String (ou String[]), ils n'existent plus une fois que la réponse a été renvoyée au WebBrowser.
- ```Request```: attributs attachés à une requêtes HTML, les objets contenus peuvent être complexes, durée de vie le temps de la requête.
- ```Session```: attributs attachés à un espace de mémoire persistant durant toute la session d'un utilisateur sur un serveur Web. Cet espace de mémoire est propre à chaque utilisateur et peut contenir des objets complexes.
- ```Application context```: attributs attachés à un espace de mémoire persistant durant toute la durée de vie du serveur Web (tant qu'il n'a pas été redémarré). Cet espace de mémoire est accessible depuis tous les composants du serveur JEE et peut contenir des objets complexes.


### 4.2 Java Bean

### 4.2.1 Définition

- les JavaBean sont des objets java qui doivent satisfaire certaines contraintes afin de pouvoir être utilisés par les composants JEE (e.g Servlet, JSP) par l'intermédiaire des espaces d'échange mis à disposition. Ce sont des composants accessibles réutilisables.
  - Les contraintes qu'un JavaBean doit respecter sont les suivantes:
    - Constructeur sans paramètre 
    - Doit être sérialisable implements Serializable
    - Accesseur aux propriétés normalisées: attribut=nom, méthode d’accès getNom (),setNom (String s)
    - Peuvent émettre des évènements en gérant une liste de listener

### 4.2.2 Création d'un JavaBean
- Créer un package ```com.sample.model``` dans le répertoire ```src```.
- Créer la classe Java ```PoneyBean.java``` comme suit:

```java
package com.sample.model;

import java.io.Serializable;

public class PoneyBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String color;
	private String superPower;
	private String name;
	private String imgUrl;

	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public PoneyBean() {
		this.color = "";
		this.superPower = "";
		this.name = "";
		this.imgUrl="";
	}
	public PoneyBean(String name,String color,String superPower, String imgUrl) {
		this.color = color;
		this.superPower = superPower;
		this.name = name;
		this.imgUrl=imgUrl;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSuperPower() {
		return superPower;
	}
	public void setSuperPower(String superPower) {
		this.superPower = superPower;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;	
	}
}

```

### 4.2.3 Création d'un Data Access Objet (DAO)
- Créer le package ```com.sample.controler``` dans le répertoire ```src```.
- Créer la Classe Java comme suit:

```java
package com.sample.controler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.sample.model.PoneyBean;

public class PoneyDao {
	private List<PoneyBean> myPoneyList;
	private Random randomGenerator;
	
	public PoneyDao() {
		myPoneyList=new ArrayList<>();
		randomGenerator = new Random();
		createPoneyList();
	}

	private void createPoneyList() {
		
		PoneyBean p1=new PoneyBean("John", "pink", "super pink", "http://ekladata.com/9-cPSlYvrenNHMVawFmf_gLx8Jw.gif");
		PoneyBean p2=new PoneyBean("Roberto", "blue", "super lazy", "http://ekladata.com/JEVyY9DkwX4vVkakeBfikSyPROA.gif");
		PoneyBean p3=new PoneyBean("Anna", "pink", "super music girl", "http://ekladata.com/fMJl--_v-3CmisaynTHju1DMeXE.gif");
		PoneyBean p4=new PoneyBean("Angry Joe", "purple", "super angry power", "http://ekladata.com/AmbNNNvv-4YFEMZR8XD8e54WoHc.gif");
		PoneyBean p5=new PoneyBean("Ursula", "pink", "super cloning power", "http://ekladata.com/CXJhi2YLUbNz6__e0Ct6ZP-XOds.gif");
		
		myPoneyList.add(p1);
		myPoneyList.add(p2);
		myPoneyList.add(p3);
		myPoneyList.add(p4);
		myPoneyList.add(p5);
	}
	public List<PoneyBean> getPoneyList() {
		return this.myPoneyList;
	}
	public PoneyBean getPoneyByName(String name){
		for (PoneyBean poneyBean : myPoneyList) {
			if(poneyBean.getName().equals(name)){
				return poneyBean;
			}
		}
		return null;
	}
	public PoneyBean getRandomPoney(){
		int index=randomGenerator.nextInt(this.myPoneyList.size());
		return this.myPoneyList.get(index);
	}
}
```

- Explications:
  - Un DAO est un pattern Java permettant d'isoler le traitement métier du traitement de la persistance des données (e.g lien avec la base de données). 
    ```java 
    ...
    private void createPoneyList() {
            
            PoneyBean p1=new PoneyBean("John", "pink", ...
            ...
            myPoneyList.add(p1);
            ...
    ``` 
  - Création "en dur " de données pour simuler une source d'information.
    ```java
    ...
    public PoneyBean getPoneyByName(String name){
        ...

    }
    ```
  - Méthode permettant de rechercher un Object PoneyBean dans la liste et de le retourner

    ```java
	public PoneyBean getRandomPoney(){
		int index=randomGenerator.nextInt(this.myPoneyList.size());
		return this.myPoneyList.get(index);
	}
    ```
  - Méthode permettant de retourner un object PoneyBean aléatoire

---

## 5 Communication Servlet JSP avec des JavaBean

### 5.1 Communication simple à l'aide de Request
#### 5.1.1 Création d'un Servlet
- Créer le package ```com.sample.servlet``` dans le répertoire ```src```.
- Créer le Servlet Java ```RedirectByNameServlet.java``` comme suit:

```java
package com.sample.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.controler.PoneyDao;
import com.sample.model.PoneyBean;

@WebServlet("/byName")
public class RedirectByNameServlet extends HttpServlet {
	private static final String NPONEY = "nponey";
	private static final String NDAO = "DAO";
	private static final long serialVersionUID = 1L;
	private PoneyDao dao;
       
    public RedirectByNameServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getDao();
		PoneyBean randPoney=this.dao.getPoneyByName(request.getParameter("name"));
		request.setAttribute(NPONEY, randPoney);
		this.getServletContext().getRequestDispatcher( "/WEB-INF/displayByName.jsp" ).forward( request, response );
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		//DO NOTHING
	}
	
	public void getDao(){
		if(this.getServletContext().getAttribute(NDAO)!=null){
			this.dao=(PoneyDao)this.getServletContext().getAttribute(NDAO);
		}else{
			this.dao=new PoneyDao();
			this.getServletContext().setAttribute(NDAO,this.dao);
		}
	}
}
```
- Explications:

    ```java
    ...
    @WebServlet("/byName")
    ...
    ```
    - Redéfinit l'url par laquelle sera accessible la servlet courante

    ```java
    ...
    public class RedirectByNameServlet extends HttpServlet {
    ...
    ```
    - La classe courante hérite de HttServlet et pourra recevoir des requêtes HTTP

    ```java
    ...
    private static final String NPONEY = "nponey";
    private static final String NDAO = "DAO";
    ...
    ```
    - Permet de définir une constante. ```static``` indique que la variable est accessible sans instancier la classe courante, ```final``` indique qu'une fois la variable instanciée elle ne pourra plus être modifiée.

    ```java
    ...
	public void getDao(){
		if(this.getServletContext().getAttribute(NDAO)!=null){
			this.dao=(PoneyDao)this.getServletContext().getAttribute(NDAO);
		}else{
			this.dao=new PoneyDao();
			this.getServletContext().setAttribute(NDAO,this.dao);
		}
	}
    ...
    ```
    - Cette méthode va permettre de créer un DAO si il n'existe pas ou de le récupérer dans un espace de mémoire partagé si il existe.
    - ```this.getServletContext()``` récupère un espace de mémoire partagé (type clé-valeur) par tous les objets du WebContainer JEE.
    - ```this.getServletContext().getAttribute(NDAO)``` permet de récupérer l'objet à la clé ```NDAO``` dans l'espace de mémoire  ```getServletContext()``` (```Application context```).

    ```java
    ...
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getDao();
		PoneyBean randPoney=this.dao.getPoneyByName(request.getParameter("name"));
		request.setAttribute(NPONEY, randPoney);
		this.getServletContext().getRequestDispatcher( "/WEB-INF/displayByName.jsp" ).forward( request, response );
	}
    ...
    ```
    - ```getDao();``` : permet de récupérer une instance du Dao
    - ```this.dao.getPoneyByName(request.getParameter("name"));``` permet de récupérer un objet Java PoneyBean par son nom envoyé en paramètre de la requête HTTP.
    - ```request.setAttribute(NPONEY, randPoney);``` va associer la clé ```NPONEY``` à l'objet ```randPoney``` dans l'espace de mémoire ```request``` (hérité de HTTPServlet). Cette donnée ne sera valable que le temps de la requête.
    - ```this.getServletContext().getRequestDispatcher( "/WEB-INF/displayByName.jsp" ).forward( request, response );``` délègue le traitement de la requête courante à la JSP ```displayByName.jsp```.

#### 5.1.2 Création d'une JSP
- Dans le répertoire ```WebContent/WEB-INF``` créer la JSP ```displayByName.jsp``` comme suit:

```html
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My poney ${requestScope.nponey.name}</title>
</head>
<body>

	<h1>This is your ${requestScope.nponey.name} PONEY!</h1>
	<h2>Color: ${requestScope.nponey.color}</h2>
	<h2>Power: ${requestScope.nponey.superPower}</h2>
	<img src=${requestScope.nponey.imgUrl} >

</body>
</html>
```
- Explications:
    ```html
    ...
        ... ${requestScope.nponey.name} ...
        ... ${requestScope.nponey.color} ...
        ... ${requestScope.nponey.superPower} ...
        ... ${requestScope.nponey.imgUrl} ...
    ...
    ```
  - ```requestScope``` : permet de récupérer l'espace de mémoire request
  - ```requestScope.nponey``` : permet de récupérer l'objet contenu à la clé ```nponey```.
  - ```requestScope.nponey.name``` : permet de récupérer l'attribut ```name``` de l'objet contenu à la clé ```nponey```. La récupération de cet attribut est possible uniquement si l'objet est un java bean serailizable et possède des accesseurs normalisés (ici ```getName()``` sera appelé)

- Tips: Le répertoire ```WebContent/WEB-INF``` n'est accessible que par le serveur. Tous les fichiers présents à l'intérieur ne pourront pas être appelés directement par un Web Browser. Seul le serveur JEE peut récupérer le contenu de ce répertoire et l'envoyer.

#### 5.1.3 Tester votre code
- Lancer le serveur JEE et appeler l'url suivante depuis un Web Browser:
```
http://localhost:8080/JSP-Servlet-JavaBean-Sample/byName?name=Ursula
```
- le résultat suivant doit apparaitre 

<p align="center"> 
    <img alt="Kitchen Vs JEE 1" src="./images/ResultByNameJSP.jpg" width="400">
</p>

### 5.2 Communication simple à l'aide de l'espace Session
#### 5.2.1 Création d'un servlet
- Dans le package ```com.sample.servlet``` créer la servlet ```RedirectRandomServlet.java``` comme suit:

```java
package com.sample.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sample.controler.PoneyDao;
import com.sample.model.PoneyBean;

@WebServlet("/random")
public class RedirectRandomServlet extends HttpServlet {
	private static final String RPONEY = "rponey";
	private static final String NDAO = "DAO";
	private static final long serialVersionUID = 1L;
	private PoneyDao dao;
       
    public RedirectRandomServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getDao();
		PoneyBean randPoney=this.dao.getRandomPoney();
		HttpSession currentSession = request.getSession();
		currentSession.setAttribute(RPONEY, randPoney);
		this.getServletContext().getRequestDispatcher( "/WEB-INF/displayRandom.jsp" ).forward( request, response );
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		//DO NOTHING
	}
	
	public void getDao(){
		if(this.getServletContext().getAttribute(NDAO)!=null){
			this.dao=(PoneyDao)this.getServletContext().getAttribute(NDAO);
		}else{
			this.dao=new PoneyDao();
			this.getServletContext().setAttribute(NDAO,this.dao);
		}
	}
}
```
- Explications:
    ```java
    ...
    HttpSession currentSession = request.getSession();
    ...
    ```
  - L'espace de mémoire session de la connexion courante est récupérée
    
    ```java
    ...
    currentSession.setAttribute(RPONEY, randPoney);    
    ...
    ```
  - ```currentSession.setAttribute(RPONEY, randPoney); ``` va associer la clé ```RPONEY``` à l'objet ```randPoney``` dans l'espace de mémoire ```session```. Cette donnée sera disponible pendant toute la session de l'utilisateur sur le Web Server.

  - ```this.getServletContext().getRequestDispatcher( "/WEB-INF/displayRandom.jsp" ).forward( request, response );``` délègue le traitement de la requête courante à la JSP ```displayRandom.jsp```.

#### 5.2.2 Création d'une JSP
- Dans le répertoire ```WebContent/WEB-INF``` créer la JSP ```displayRandom.jsp``` comme suit:

```html
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My Random Poney</title>
</head>
<body>

	<h1>WELCOME TO ${sessionScope.rponey.name} PONEY!</h1>
	<h2>Color: ${sessionScope.rponey.color}</h2>
	<h2>Power: ${sessionScope.rponey.superPower}</h2>
	<img src=${sessionScope.rponey.imgUrl} >

</body>
</html>

```
- Explications:
    ```html
    ... ${sessionScope.rponey.name} ...
	... ${sessionScope.rponey.color} ...
	... ${sessionScope.rponey.superPower} ...
	... ${sessionScope.rponey.imgUrl} ...
    ```
    - ```sessionScope```:  permet de récupérer l'espace de mémoire session
  - ```sessionScope.rponey``` : permet de récupérer l'objet contenu à la clé ```rponey```.
  - ```sessionScope.rponey.superPower``` : permet de récupérer l'attribut ```superPower``` de l'objet contenu à la clé ```rponey```. La récupération de cet attribut est possible uniquement si l'objet est un java bean serailizable et possède des accesseurs normalisés (ici ```getSuperPower()``` sera appelé)

#### 5.2.3 Tester votre code
- Lancer le serveur JEE et appeler l'url suivante depuis un Web Browser:
```
http://localhost:8080/JSP-Servlet-JavaBean-Sample/random
```
- le résultat suivant doit apparaitre : 

<p align="center"> 
    <img alt="Result JEE Random" src="./images/ResultRandomJSP.jpg" width="400">
</p>


### 5.3 Communication simple à l'aide de l'espace Application
#### 5.3.1 Création d'un Servlet
- Dans le package ```com.sample.servlet``` créer le Servlet ```RedirectListServlet``` comme suit:

```java
package com.sample.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.controler.PoneyDao;
import com.sample.model.PoneyBean;

@WebServlet("/list")
public class RedirectListServlet extends HttpServlet {
	private static final String LPONEY = "lponey";
	private static final String NDAO = "DAO";
	private static final long serialVersionUID = 1L;
	private PoneyDao dao;
       
    public RedirectListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getDao();
		List<PoneyBean> listPoney=this.dao.getPoneyList();
		
		this.getServletContext().setAttribute(LPONEY, listPoney);
		
		this.getServletContext().getRequestDispatcher( "/WEB-INF/displayList.jsp" ).forward( request, response );
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		//DO NOTHING
	}
	
	public void getDao(){
		if(this.getServletContext().getAttribute(NDAO)!=null){
			this.dao=(PoneyDao)this.getServletContext().getAttribute(NDAO);
		}else{
			this.dao=new PoneyDao();
			this.getServletContext().setAttribute(NDAO,this.dao);
		}
	}
}
```

- Explications:
  ```java
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getDao();
		List<PoneyBean> listPoney=this.dao.getPoneyList();
		
		this.getServletContext().setAttribute(LPONEY, listPoney);
		
		this.getServletContext().getRequestDispatcher( "/WEB-INF/displayList.jsp" ).forward( request, response );
	}
    ```
   - ```this.getServletContext().setAttribute(LPONEY, listPoney);``` va associer la clé ```LPONEY``` à l'objet ```listPoney``` dans l'espace de mémoire ```application``` (fournit par ```this.getServletContext()```). Cette donnée sera disponible pendant toute la durée de vie du Server Web (jusqu'au prochain redémarrage), cette donnée sera accessible par tous les objets (jsp,servlet) du serveur Jee.

  - ```this.getServletContext().getRequestDispatcher( "/WEB-INF/displayList.jsp" ).forward( request, response );``` délègue le traitement de la requête courante à la JSP ```displayList.jsp```.

#### 5.3.2 Création d'une JSP
- Dans le répertoire ```WebContent/WEB-INF``` créer la JSP ```displayList.jsp``` comme suit:

```html
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of Poneys</title>
</head>
<body>

	<c:forEach items="${applicationScope.lponey}" var="poney">
		<h1>This is your ${poney.name} PONEY!</h1>
		<h2>Color: ${poney.color}</h2>
		<h2>Power: ${poney.superPower}</h2>
		<img src=${poney.imgUrl} height="80" width="80">
		<hr>
	</c:forEach>
</body>
</html>
```

- Explications:
```html
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
```
- Import d'un namespace supplémentaire permettant l'ajout de balise (ici fournit par la librairie JSTL)
    ```html
    ...
    ${applicationScope.lponey}
    ...
    ```
  - ```applicationScope```:  permet de récupérer l'espace de mémoire application
  - ```applicationScope.lponey``` : permet de récupérer l'objet (une liste dans notre cas) contenu à la clé ```lponey```.

    ```html
    ...
	<c:forEach items="${applicationScope.lponey}" var="poney">
		<h1>This is your ${poney.name} PONEY!</h1>
		<h2>Color: ${poney.color}</h2>
		<h2>Power: ${poney.superPower}</h2>
		<img src=${poney.imgUrl} height="80" width="80">
		<hr>
    </c:forEach>
    ...
    ```
  - ```<c:forEach items="${applicationScope.lponey}" var="poney">``` : utilitaire JSTL permettant d'itérer sur les objets d'une liste. Ici la list à iterer est définit dans ``` items= ... ```. L'élément courant à traiter durant l'itération sera contenu dans la variable ```poney``` via ```var="poney"```.
    ```html
    <h2>Color: ${poney.color}</h2>
    ```
  - ```${poney.color}```:  Utilise la variable poney qui contient l'objet de l'itération courante et va chercher l'attribut ```color``` de cette variable. Cet attribut est accessible si l'objet en question est un JavaBean sérializable et possède des accesseurs normalisés (ici ```getColor()``` sera utilisé).

#### 5.3.3 Tester votre code
- Lancer le serveur JEE et appeler l'url suivante depuis un Web Browser:
```
http://localhost:8080/JSP-Servlet-JavaBean-Sample/list
```
- le résultat suivant doit apparaitre :

<p align="center"> 
    <img alt="Result JEE Random" src="./images/ResultListJSP.jpg" width="400">
</p>
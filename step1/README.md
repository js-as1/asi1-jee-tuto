# Mise en oeuvre de Servlet dans un projet JEE

## 1. Contexte

Les serveurs JEE sont des serveurs applicatifs qui permettent de faire des applications Web (via Web Tiers appelé aussi le Web Container) et faire des applications M2M (Business Tiers appelé aussi  EJB Container). Une description complète des serveurs JEE est disponible ici (https://javaee.github.io/tutorial/overview001.html).

<p align="center"> 
<img alt="JEE architecture" src="https://docs.oracle.com/javaee/7/tutorial/img/jeett_dt_001.png" width="300">
</p>
Aussi une machine cliente peut interagir avec le serveur JEE de plusieurs façons:
- Via un Web Browser en utilisant HTTP: le Web Browser va ainsi interagir avec le Tiers Web du serveur JEE qui pourra lui répondre soit par le biais applications REST soit en utilisant des pages Web statiques (*.html) ou dynamiques (*.jsp).
- Via une application java (ou autre) au travers des protocoles M2M (e.g RMI): l'application va alors interagir avec le Business Tiers du Serveur JEE.

<p align="center"> 
<img alt="JEE WebContainer Archtecture" src="../step1/images/webContainer-archi.jpg" width="200">
</p>


Dans cette partie nous nous intéressons essentiellement aux applications Web. Aussi seul le Tiers Web ou Web Container va être utilisé.
Lorsqu'un Web Browser va interagir avec le Tiers Web du serveur JEE, ce dernier va envoyer une requête HTTP qui va être interceptée par le serveur HTTP du serveur JEE.
Le serveur JEE va alors contacter un Object java particulier, un ```Servlet```, qui va définir le comportement à avoir face à cette requête HTTP. Pour traiter l'information le Servlet (en java) va pouvoir utiliser toutes les libraires java et JEE disponibles.
Le ```Servlet``` pourra alors traiter la requête et effectuer un retour direct au Web Browser. Elle pourra tout comme PHP remplir des templates de pages HTML, des fichiers ```*.jsp```, et retourner le flux HTML au Web Browser.

---

## 2. Création d'une Application JEE
En vous basant sur le ```step0```. Créer un Projet JEE Web Dynamic du nom ```JEE-Servlet-Example```

---

## 3. Création d'un premier Servlet

- Dans ```Java Resources\src``` créer le package suivant (clic droit new->package) :
```
com.cpe.tuto.jee.servlet

```
- Sélectionner le package créé et créer un Servlet (clic droit->new->Servlet) ```HelloServlet```

    ```java
    package com.cpe.tuto.jee.servlet;

    import java.io.IOException;
    import javax.servlet.ServletException;
    import javax.servlet.annotation.WebServlet;
    import javax.servlet.http.HttpServlet;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;

    @WebServlet("/HelloServlet")
    public class HelloServlet extends HttpServlet {
        private static final long serialVersionUID = 1L;
        
        public HelloServlet() {
            super();
        }

        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            response.getWriter().append("Served at: ").append(request.getContextPath());
        }

        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            doGet(request, response);
        }
    }
    ```
- Explications:
    ```java
    ...
    @WebServlet("/HelloServlet")
    ...
    ```
    - Cette annotation permet d'indiquer quelle sera l'URL à appeler pour déclencher cette servlet. Ici l'URL suivante déclenchera notre servlet  ```http://localhost:8080/JEE-Servlet-Example/HelloServlet```

    ```java
    ...
    public class HelloServlet extends HttpServlet {
    ...
    ```
    - Cette ligne indique que la classe java courante hérite de toutes les propriétés de la classe ```HttpServlet``` et pourra ainsi être déclenchée par le biais de requête HTTP.

    ```java
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            ...
        }

        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            ...
        }
    ```
  - Ces méthodes java vont être déclenchées lors de l'appel de l'URL du servlet courant ```/HelloServlet``` avec la méthode Http ```GET``` (```doGet``` ) ou Http ```POST``` (```doPost``` ). Chacune de ces méthodes (```doGet```, ```doPost```) a accès aux éléments de la requête Http dans le paramètre ```HttpServletRequest request```, et aux outils pour répondre à la requête dans la variable  ```HttpServletResponse response```.
<p align="center"> 
<img alt="elt Http request" src="./images/HTTPRequest.jpg" width="200">
<img alt="elt Http request" src="./images/HTTPResponse.jpg" width="200">
</p>
- Démarrer votre application :
  - clic droit sur le fichier ```HelloServlet.java```
  - ``` Run AS -> Run On Server ```
  - le résultat suivant doit être obtenu

<p align="center"> 
<img alt="HelloServlet Result" src="images/HelloServletResult.jpg" width="400">
</p>

---

## 4. Création d'un Servlet, attributs, paramètres et retours
### 4.1 Création d'un Servlet
- Dans le package ```com.cpe.tuto.jee.servlet``` créer un nouveau Servlet ```Test1Servlet```.

### 4.3 Modification de l'URL d'accès
- Modifier l'annotation ```@WebServlet("/Test1Servlet")``` par ```@WebServlet("/test1")```
- Votre Servlet sera alors accessible via l'URL ```http://localhost:8080/JEE-Servlet-Example/test1```

### 4.4 Récupération de paramètres dans doGet
- Modifier la fonction ```doGet``` de votre Servlet ```Test1Servlet``` comme suit:

    ```java
    ...
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            
            String name_value = request.getParameter("name");
            String result="";
            
            for( Entry<String, String[]> att_entry : request.getParameterMap().entrySet()) {
                result= result +"<h3>"+ "["+att_entry.getKey()+"]:"+att_entry.getValue()[0]+"</h3>";
            }
            
            PrintWriter writer = response.getWriter();
            writer.println("<h1> The name param value is:"+name_value+"</h1>");
            writer.println("<h2> Below the list of sent params:</h2>");
            writer.println(result);
        }
    ...

    ```

- Explications:

    ```java
    String name_value = request.getParameter("name");
    ```
    - Ici nous utilisons le HttpServletResquest pour récupérer le paramètre ```name``` associé à l'URL.
  
    ```java
    for( Entry<String, String[]> att_entry : request.getParameterMap().entrySet()) {
			result= result + "["+att_entry.getKey()+"]:"+att_entry.getValue()[0]+"\n";
	}
    ```
    - Nous récupérons l'ensemble des paramètres associés à l'URL ```request.getParameterMap()```et nous itérons sur la map afin de récupérer le nom du paramètre (```att_entry.getKey()```) et la valeur associée (``` att_entry.getValue()[0] ```)

    ```java
    ...
    PrintWriter writer = response.getWriter();
	writer.println(" The name param value is:"+name_value);
    ...
    ```
  - L'HttpServletResponse nous permet d'écrire dans le flux de réponse http. ```response.getWriter();``` nous permet d'écrire des données qui vont être transmises au Web Browser. L'information transmise étant interprétée par le Web Browser nous pouvons ajouter des balises HTML ```writer.println("<h2> Below the list of sent params:</h2>");``` afin de mettre à jour l'affichage.



- Exécuter l'application puis entrer l'URL suivante dans un Web Browser
    ```
    http://localhost:8080/JEE-Servlet-Example/test1
    ```

- Vous devez avoir le résultat suivant:

    ```
    The name param value is:null

    Below the list of sent params:
    ```


- Exécuter l'application puis entrer l'URL suivante dans un Web Browser
    ```
    http://localhost:8080/JEE-Servlet-Example/test1?name=jdoe&pwd=jdoepwd&email=jdoe@company.com
    ```

- Vous devez avoir le résultat suivant:
    ```
    The name param value is:jdoe

    Below the list of sent params:

    [name]:jdoe
    [pwd]:jdoepwd
    [email]:jdoe@company.com
    ```

### 4.5 Récupération de paramètres depuis un form dans doPost
- Modifier la fonction ```doPost``` de votre Servlet ```Test1Servlet``` comme suit:

    ```java
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            String result="";
            for( Entry<String, String[]> att_entry : request.getParameterMap().entrySet()) {
                result= result +"<h3>"+ "["+att_entry.getKey()+"]:"+att_entry.getValue()[0]+"</h3>";
            }
            
            PrintWriter writer = response.getWriter();
            writer.println("<h1> Below the list of sent params:</h2>");
            writer.println(result);
        }

    ```

- Dans le répertoire ```WebContent``` ajouter le fichier ```form.html``` suivant :

    ```html
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="ISO-8859-1">
    <title>MyForm</title>
    </head>
    <body>

        <form name="myForm" method="post" action="test1">
            Prefered Color: <input type="text" name="color" /> <br /> Super Power:
            <input type="text" name="power" /> <br /> Family: <input type="text"
                name="family" /> <br /> <input type="submit" value="save" />
        </form>

    </body>
    </html>
    ```

- Exécuter votre application et appeler l'url suivante dans votre Web Browser

    ```
    http://localhost:8080/JEE-Servlet-Example/form.html
    ```

- En fonction des éléments saisis dans le formulaire, le résultat obtenu doit avoir la forme suivante:
  
    ```
    Below the list of sent params:
    [color]:blue
    [power]:fly
    [family]:DC
    ```
---

## 5 Redirection et Forward

### 5.1 Création de Servlets et mise en place de sendRedirect
- Dans ```com.cpe.tuto.jee.servlet``` créer les 3 Servlets suivants:
  - ```Redirect1Servlet```
  - ```Redirect2Servlet```
  - ```Redirect3Servlet```


- Modifier le contenu de ```Redirect1Servlet``` comme suit:
  
  ```java
    package com.cpe.tuto.jee.servlet;

    import java.io.IOException;
    import javax.servlet.ServletException;
    import javax.servlet.annotation.WebServlet;
    import javax.servlet.http.HttpServlet;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;

    @WebServlet("/redirect1")
    public class Redirect1Servlet extends HttpServlet {
        private static final long serialVersionUID = 1L;
        
        public Redirect1Servlet() {
            super();
        }

        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            System.out.println(" I AM REDIRECT1 SERVLET");
            response.sendRedirect("redirect2");
        }

    }
  ```

- Modifier le contenu de ```Redirect2Servlet``` comme suit:
  
  ```java
    package com.cpe.tuto.jee.servlet;

    import java.io.IOException;
    import javax.servlet.ServletException;
    import javax.servlet.annotation.WebServlet;
    import javax.servlet.http.HttpServlet;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;

    @WebServlet("/redirect2")
    public class Redirect2Servlet extends HttpServlet {
        private static final long serialVersionUID = 1L;
        
        public Redirect2Servlet() {
            super();
        }

        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            System.out.println(" I AM REDIRECT2 SERVLET");
            response.sendRedirect("redirect3");
        }
    }
  ```

- Modifier le contenu de ```Redirect3Servlet``` comme suit:
  
  ```java
    package com.cpe.tuto.jee.servlet;

    import java.io.IOException;
    import javax.servlet.ServletException;
    import javax.servlet.annotation.WebServlet;
    import javax.servlet.http.HttpServlet;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;

    @WebServlet("/redirect3")
    public class Redirect3Servlet extends HttpServlet {
        private static final long serialVersionUID = 1L;
        
        public Redirect3Servlet() {
            super();
        }

        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            System.out.println(" I AM REDIRECT3 SERVLET");
            response.sendRedirect("redirect.html");
        }
    }
  ```

  - Ajouter le fichier ```redirect.html``` dans le répertoire ```WebContent```:
  ```html
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="ISO-8859-1">
    <title>Redirect</title>
    </head>
    <body>
        <h1>The redirection is finished</h1>
    </body>
    </html>
  ```

### 5.2 Execution de senRedirect
- Executer votre application JEE
- Lancer un navigateur avec l'URL suivante:
    ```
    http://localhost:8080/JEE-Servlet-Example/redirect1
    ```

- Dans la console du serveur vous devriez avoir la sortie suivante:
    ```
    I AM REDIRECT1 SERVLET
    I AM REDIRECT2 SERVLET
    I AM REDIRECT3 SERVLET
    ```

- La réponse sur serveur devrait vous donner le résultat suivante avec l'URL visible ```http://localhost:8080/JEE-Servlet-Example/redirect.html ```:

    ```
    The redirection is finished
    ```

- Explications
  
    ```java
     response.sendRedirect("redirect2");
    ```
    -   la commande ```sendRedirect``` permet d'envoyer une requête au navigateur Web afin qu'il soit redirigé vers une autre URL (code HTTP 302). Lors de cette demande de redirection l'ensemble du contenu et des paramètres de la requète originale sont perdus. 
    -   Il est possible également de voir l'URL utilisée par le serveur pour que le Web Browser atteigne une ressource (```http://localhost:8080/JEE-Servlet-Example/redirect.html```)

### 5.3 Mise en place de Forward
- Dans ```com.cpe.tuto.jee.servlet``` créer les 2 servlets suivantes:
  - ```Redirect4Servlet```
  - ```Redirect5Servlet```

- Modifier le contenu de ```Redirect4Servlet``` comme suit:
    ```java
    package com.cpe.tuto.jee.servlet;

    import java.io.IOException;
    import java.util.Map.Entry;

    import javax.servlet.RequestDispatcher;
    import javax.servlet.ServletException;
    import javax.servlet.annotation.WebServlet;
    import javax.servlet.http.HttpServlet;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;

    @WebServlet("/redirect4")
    public class Redirect4Servlet extends HttpServlet {
        private static final long serialVersionUID = 1L;
        
        public Redirect4Servlet() {
            super();
        }

        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            System.out.println("redirect4");
            for( Entry<String, String[]> att_entry : request.getParameterMap().entrySet()) {
                System.out.println( "["+att_entry.getKey()+"]:"+att_entry.getValue()[0]);
            }
            
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("redirect5");
            requestDispatcher.forward(request, response);
        }
    }
    ```

- Modifier le contenu de ```Redirect5Servlet``` comme suit:
    ```java
    package com.cpe.tuto.jee.servlet;

    import java.io.IOException;
    import java.util.Map.Entry;

    import javax.servlet.RequestDispatcher;
    import javax.servlet.ServletException;
    import javax.servlet.annotation.WebServlet;
    import javax.servlet.http.HttpServlet;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;

    @WebServlet("/redirect5")
    public class Redirect5Servlet extends HttpServlet {
        private static final long serialVersionUID = 1L;
        
        public Redirect5Servlet() {
            super();
        }

        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            System.out.println("redirect5");
            for( Entry<String, String[]> att_entry : request.getParameterMap().entrySet()) {
                System.out.println( "["+att_entry.getKey()+"]:"+att_entry.getValue()[0]);
            }
            
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("redirect.html");
            requestDispatcher.forward(request, response);
        }
    }

    ```

### 5.4 Execution de Forward
- Executer votre application JEE
- Lancer un navigateur avec l'URL suivante:
    ```
    http://localhost:8080/JEE-Servlet-Example/redirect4?name=jdoe&pwd=jdoepwd&email=jdoe@company.com
    ```

- Dans la console du serveur vous devriez avoir la sortie suivante:
    ```
    redirect4
    [name]:jdoe
    [pwd]:jdoepwd
    [email]:jdoe@company.com
    redirect5
    [name]:jdoe
    [pwd]:jdoepwd
    [email]:jdoe@company.com
    ```

- La réponse sur serveur devrait vous donner le résultat suivant avec l'URL visible ```http://localhost:8080/JEE-Servlet-Example/redirect4... ``` identique à celle envoyée:

    ```
    The redirection is finished
    ```
- Explications

    ```java
    ...
    RequestDispatcher requestDispatcher = request.getRequestDispatcher("redirect.html");
    requestDispatcher.forward(request, response);
    ...
    ```
  - ```RequestDispatcher``` permet de retransférer l'ensemble de la requête à une autre URL **au sein du serveur**. Le Web Browser n'est pas au courant de ce retransfert interne de requêtes. C'est pourquoi l'URL visible sur le Web Browser est inchangée et que seul le contenu final est affiché.